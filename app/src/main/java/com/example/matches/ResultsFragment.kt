package com.example.matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.matches.adapter.ResultsRecyclerViewAdapter
import com.example.matches.databinding.FragmentResultsBinding
import com.example.matches.viewmodel.ResultsViewModel

class ResultsFragment : Fragment() {

    private val viewModel: ResultsViewModel by lazy {
        ViewModelProviders.of(this).get(ResultsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentResultsBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.viewmodel = viewModel

        binding.resultsRecyclerView.adapter =

            ResultsRecyclerViewAdapter(ResultsRecyclerViewAdapter.OnClickListener {

            })


        return binding.root
    }
}