package com.example.matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.matches.adapter.FixturesRecyclerViewAdapter
import com.example.matches.databinding.FragmentFixturesBinding
import com.example.matches.viewmodel.FixturesViewModel

class FixturesFragment : Fragment() {

    private val viewModel: FixturesViewModel by lazy {
        ViewModelProviders.of(this).get(FixturesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentFixturesBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.viewmodel = viewModel


        binding.fixturesRecyclerView.adapter =
            FixturesRecyclerViewAdapter(FixturesRecyclerViewAdapter.OnClickListener {
                //Ideally would go to a fixture details
            })

        return binding.root
    }
}