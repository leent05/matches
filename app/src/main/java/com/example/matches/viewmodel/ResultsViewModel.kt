package com.example.matches.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.matches.api.model.Results
import com.example.matches.api.networking.MatchesApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*

enum class ResultsApiStatus { LOADING, ERROR, DONE }


class ResultsViewModel : ViewModel() {

    private val _status = MutableLiveData<ResultsApiStatus>()

    val status: LiveData<ResultsApiStatus>
        get() = _status

    private val _results = MutableLiveData<List<Results>>()

    val results: LiveData<List<Results>>
        get() = _results

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getAllResults()
    }

    private fun getAllResults() {
        coroutineScope.launch {
            var getResultsDeferred = MatchesApi.retrofitService.getResultsList()
            try {
                _status.value = ResultsApiStatus.LOADING
                val listFixtures = getResultsDeferred.await()
                _status.value = ResultsApiStatus.DONE
                _results.value = listFixtures

            } catch (networkError: IOException) {
                _status.value = ResultsApiStatus.ERROR
                _results.value = ArrayList()

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}