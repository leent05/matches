package com.example.matches.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.matches.api.model.Fixtures
import com.example.matches.api.networking.MatchesApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.IOException

enum class FixturesApiStatus { LOADING, ERROR, DONE }

class FixturesViewModel : ViewModel() {

    private val _status = MutableLiveData<FixturesApiStatus>()

    val status: LiveData<FixturesApiStatus>
        get() = _status

    private val _fixtures = MutableLiveData<List<Fixtures>>()

    val fixtures: LiveData<List<Fixtures>>
        get() = _fixtures

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getAllFixtures()
    }


    private fun getAllFixtures() {
        coroutineScope.launch {
            var getFixturesDeferred = MatchesApi.retrofitService.getFixturesList()
            try {
                _status.value = FixturesApiStatus.LOADING
                val listFixtures = getFixturesDeferred.await()
                _status.value = FixturesApiStatus.DONE
                _fixtures.value = listFixtures

            } catch (networkError: IOException) {
                _status.value = FixturesApiStatus.ERROR
                _fixtures.value = ArrayList()

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}