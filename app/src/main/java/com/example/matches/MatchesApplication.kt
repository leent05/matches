package com.example.matches

import android.app.Application
import timber.log.Timber

class MatchesApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}