package com.example.matches.utils

import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.matches.adapter.FixturesRecyclerViewAdapter
import com.example.matches.adapter.ResultsRecyclerViewAdapter
import com.example.matches.api.model.Fixtures
import com.example.matches.api.model.Results
import com.example.matches.viewmodel.FixturesApiStatus
import com.example.matches.viewmodel.ResultsApiStatus

@BindingAdapter("listFixtures")
fun bindFixturesRecyclerView(recyclerView: RecyclerView, data: List<Fixtures>?) {
    val adapter = recyclerView.adapter as FixturesRecyclerViewAdapter
    adapter.submitList(data)
}

@BindingAdapter("listResults")
fun bindResultsRecyclerView(recyclerView: RecyclerView, data: List<Results>?) {
    val adapter = recyclerView.adapter as ResultsRecyclerViewAdapter
    adapter.submitList(data)
}


@BindingAdapter("fixturesApiStatus")
fun bindStatus(progress: ProgressBar, status: FixturesApiStatus?) {
    when (status) {
        FixturesApiStatus.LOADING -> {
            progress.visibility = View.VISIBLE
        }
        FixturesApiStatus.ERROR -> {
            progress.visibility = View.VISIBLE
        }
        FixturesApiStatus.DONE -> {
            progress.visibility = View.GONE
        }
    }
}

@BindingAdapter("resultsApiStatus")
fun bindStatus(progress: ProgressBar, status: ResultsApiStatus?) {
    when (status) {
        ResultsApiStatus.LOADING -> {
            progress.visibility = View.VISIBLE
        }
        ResultsApiStatus.ERROR -> {
            progress.visibility = View.VISIBLE
        }
        ResultsApiStatus.DONE -> {
            progress.visibility = View.GONE
        }
    }
}