package com.example.matches.utils

import java.text.SimpleDateFormat
import java.util.*

object DateFormatingUtils {

    fun formatFixtureDate(unformattedDate: String): String {
        var formattedDate = " "
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z")
        val date: Date = inputFormat.parse(unformattedDate)
        formattedDate = outputFormat.format(date)
        return formattedDate.substring(0, formattedDate.length - 9)
    }

    fun getDayOfMonth(unformattedDate: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("dd/M/yyyy")
        val date: Date = inputFormat.parse(unformattedDate)
        return outputFormat.format(date).take(2)
    }
}