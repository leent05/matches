package com.example.matches.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.matches.R
import com.example.matches.api.model.Fixtures
import com.example.matches.databinding.FixturesItemBinding
import com.example.matches.utils.DateFormatingUtils


class FixturesRecyclerViewAdapter(val onClickListener: OnClickListener) :
    ListAdapter<Fixtures, FixturesRecyclerViewAdapter.FixturesViewHolder>(DiffCallback) {

    class FixturesViewHolder(private var binding: FixturesItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(fixtures: Fixtures) {
            binding.fixture = fixtures
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Fixtures>() {
        override fun areItemsTheSame(oldItem: Fixtures, newItem: Fixtures): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Fixtures, newItem: Fixtures): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FixturesViewHolder {
        return FixturesViewHolder(
            FixturesItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(
        holder: FixturesViewHolder,
        position: Int
    ) {
        val fixtures = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(fixtures)
        }
        if (fixtures.state == "postponed") {
            holder.itemView.findViewById<TextView>(R.id.fixtures_status).isVisible = true
        }
        holder.itemView.findViewById<TextView>(R.id.fixtures_day_of_week).text =
            DateFormatingUtils.formatFixtureDate(fixtures.date.toString()).take(3)
        holder.itemView.findViewById<TextView>(R.id.fixtures_date).text =
            " " + DateFormatingUtils.formatFixtureDate(fixtures.date.toString())
        holder.itemView.findViewById<TextView>(R.id.fixtures_day_of_month).text =
            DateFormatingUtils.getDayOfMonth(fixtures.date.toString())
        holder.bind(fixtures)
    }

    class OnClickListener(val clickListener: (fixtures: Fixtures) -> Unit) {
        fun onClick(fixtures: Fixtures) = clickListener(fixtures)
    }


}

