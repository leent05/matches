package com.example.matches.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.matches.R
import com.example.matches.api.model.Results
import com.example.matches.databinding.ResultsItemBinding
import com.example.matches.utils.DateFormatingUtils


class ResultsRecyclerViewAdapter(val onClickListener: OnClickListener) :
    ListAdapter<Results, ResultsRecyclerViewAdapter.ResultsViewHolder>(DiffCallback) {

    class ResultsViewHolder(private var binding: ResultsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(results: Results) {
            binding.results = results
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Results>() {
        override fun areItemsTheSame(oldItem: Results, newItem: Results): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Results, newItem: Results): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ResultsViewHolder {
        return ResultsViewHolder(
            ResultsItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(
        holder: ResultsViewHolder,
        position: Int
    ) {
        val results = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(results)
        }
        holder.itemView.findViewById<TextView>(R.id.results_date).text =
            " " + DateFormatingUtils.formatFixtureDate(results.date.toString())
        holder.bind(results)
    }

    class OnClickListener(val clickListener: (results: Results) -> Unit) {
        fun onClick(results: Results) = clickListener(results)
    }
}