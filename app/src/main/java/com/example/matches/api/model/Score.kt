package com.example.matches.api.model

import com.squareup.moshi.Json


data class Score(
    @Json(name = "home")
    val home: Int? = null,
    @Json(name = "away")
    val away: Int? = null,
    @Json(name = "winner")
    val winner: String? = ""
)