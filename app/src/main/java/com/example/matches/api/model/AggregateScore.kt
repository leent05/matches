package com.example.matches.api.model

import com.squareup.moshi.Json


data class AggregateScore(
    @Json(name = "home")
    private val home: Int? = null,
    @Json(name = "away")
    private val away: Int? = null,
    @Json(name = "winner")
    private val winner: String? = ""
)
