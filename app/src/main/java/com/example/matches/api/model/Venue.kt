package com.example.matches.api.model

import com.squareup.moshi.Json


data class Venue(
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "name")
    val name: String? = ""
)