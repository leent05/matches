package com.example.matches.api.model

import com.squareup.moshi.Json


data class CompetitionStage(
    @Json(name = "competition")
    val competition: Competition? = null,
    @Json(name = "stage")
    val stage: String?= "",
    @Json(name = "leg")
    val leg: String?=""
)