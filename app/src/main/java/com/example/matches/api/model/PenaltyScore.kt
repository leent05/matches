package com.example.matches.api.model

import com.squareup.moshi.Json


data class PenaltyScore(
    @Json(name = "home")
    val home: Int? = null,
    @Json(name = "away")
    val away: Int? = null,
    @Json(name = "winner")
    val winner: String? = ""
)