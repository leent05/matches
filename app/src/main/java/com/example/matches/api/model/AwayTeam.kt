package com.example.matches.api.model

import com.squareup.moshi.Json


data class AwayTeam(
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "name")
    val name: String? = "",
    @Json(name = "shortName")
    val shortName: String? = "",
    @Json(name = "abbr")
    val abbr: String? = "",
    @Json(name = "alias")
    val alias: String? = ""
)