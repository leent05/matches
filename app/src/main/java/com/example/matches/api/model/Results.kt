package com.example.matches.api.model

import com.squareup.moshi.Json

data class Results (
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "type")
    val type: String? = "",
    @Json(name = "homeTeam")
    val homeTeam: HomeTeam? = null,
    @Json(name = "awayTeam")
    val awayTeam: AwayTeam?= null,
    @Json(name = "date")
    val date: String? = "",
    @Json(name = "competitionStage")
    val competitionStage: CompetitionStage? = null,
    @Json(name = "venue")
    val venue: Venue? = null,
    @Json(name = "state")
    val state: String? = "",
    @Json(name = "score")
    val score: Score? = null,
    @Json(name = "penaltyScore")
    val penaltyScore: PenaltyScore? = null,
    @Json(name = "aggregateScore")
    val aggregateScore: AggregateScore? = null
)