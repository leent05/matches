package com.example.matches.api.model

import com.squareup.moshi.Json


data class Fixtures(
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "type")
    val type: String? = "",
    @Json(name = "homeTeam")
    val homeTeam: HomeTeam? = null,
    @Json(name = "awayTeam")
    val awayTeam: AwayTeam? = null,
    @Json(name = "date")
    val date: String? = "",
    @Json(name = "competitionStage")
    val competitionStage: CompetitionStage? = null,
    @Json(name = "venue")
    val venue: Venue? = null,
    @Json(name = "state")
    val state: String? = ""
)