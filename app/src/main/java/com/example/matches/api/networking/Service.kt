package com.example.matches.api.networking

import com.example.matches.api.model.Fixtures
import com.example.matches.api.model.Results
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


private const val BASE_URL = "https://storage.googleapis.com/cdn-og-test-api/test-task/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()


interface MatchesApiService {

    @GET("fixtures.json")
    fun getFixturesList():
            Deferred<List<Fixtures>>

    @GET("results.json")
    fun getResultsList():
            Deferred<List<Results>>
}

object MatchesApi {
    val retrofitService: MatchesApiService by lazy { retrofit.create(MatchesApiService::class.java) }
}