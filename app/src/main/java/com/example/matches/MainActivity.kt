package com.example.matches

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openFragment(FixturesFragment())

       bottom_navigation.setOnNavigationItemSelectedListener {
           when(it.itemId){

               R.id.navigation_fixtures -> {
                   openFragment(FixturesFragment())
                   return@setOnNavigationItemSelectedListener true
               }
               R.id.navigation_results -> {
                   openFragment(ResultsFragment())
                   return@setOnNavigationItemSelectedListener true
               }
           }
           false
       }
    }

    fun openFragment(fragment: Fragment) {
       val  transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}


